# Publication et installation

[Documentation Gitlab](https://docs.gitlab.com/ee/user/packages/composer_repository/)

## Publication d'une nouvelle version :

Le fichier `.gitlab-ci.yml` définit un job Gitlab qui publie une nouvelle version dans le package registry du projet
Gitlab.

Cette publication s'effectue automatiquement lorsque l'on `push` un nouveau `tag` git, par exemple :

```bash
$ git tag 2.0.4
$ git push --tags
```

Un [pipeline](https://gitlab.com/menkorn/xengine-framework/-/pipelines) est alors éxécuté, et une nouvelle version est
publiée et [disponible](https://gitlab.com/menkorn/xengine-framework/-/packages)


## Installation ##

Dans le fichier `composer.json` :

```json
{
  ...
  "repositories": {
    "1464705": {
      "type": "composer",
      "url": "https://gitlab.com/api/v4/group/1464705/-/packages/composer/packages.json"
    },
    ...
  },
  "require": {
    ...
    "crisisoft/xengine": "<version>"
  },
  ...
}
```

Créer un fichier `auth.json` :

```json
{
    "gitlab-token": {
        "gitlab.com": {
            "username": "<username>",
            "token": "<token>"
        }
    }
}
```

Puis installer le paquet :

```bash
$ composer require crisisoft/xengine
```

# Utilisation du framework

## Ligne de commande ##

```

$ cd vendor/crisisoft/xengine
$ ./console/xengine init

```

Un lien symbolique vers le script `vendor/crisisoft/xengine/console/xengine` est alors créé à la racine du projet

```

$ ./xengine [module|dao] options

```

### xengine init ###
Initialisation du projet

### xengine module [create|add|remove|redirect] moduleName (controllerName) ###

* `xengine module create moduleName` Création de l'arborescence du module 'moduleName'
* `xengine module add moduleName controllerName [controllerRedirect]` Ajoute le controller 'controllerName' au module 'moduleName'
* `xengine module remove moduleName controllerName` Supprime le controller 'controllerName' du module 'moduleName'
* `xengine module redirect moduleName` Définit le module 'moduleName' comme module par défaut dans le fichier public/index.php

### xengine dao generate [--all|modelName] [--business] [--dao] [--daocust] [--verbose] ###
* `xengine dao generate moduleName` Génère tous les DAO non générés ou bien seulement celui de 'modelName'
    - --all Tous les modèles, sans demande à l'utilisateur
    - --business Fichiers business
    - --dao Fichiers dao
    - --daocust Fichiers daoCust
    - --verbose Affiche le détail

### xengine theme add themeName ###
* `xengine theme add themeName` Génère le répertoire et les fichiers principaux du thème 'themeName'


### Autocomplétion ###

Le fichier `console/xengine.autocomplete` est disponible pour permettre l'autocomplétion des commandes.
